<?php
namespace Core;

class Route
{
	protected $routes = [
		'GET' => [],
        'POST' => []
    ];

    /**
     * Load a routes.php.
     *
     * @param string $file
     */
    public static function load($file)
    {
        $router = new static;

        require $file;

        return $router;
    }

	public  function  get($uri, $controllerName) 
	{
		 $this->routes['GET'][$uri] = $controllerName;
	}

	public  function  post($uri, $controllerName) 
	{
		 $this->routes['POST'][$uri] = $controllerName;
	}

    /**
     * Load the requested URI's associated controller method.
     *
     * @param string $uri
     * @param string $requestType
     */
    public function directToController($uri, $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            // using splat operator
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri])
            );
        }

        throw new Exception('Sorry, No Route Found.');
    }

    /**
     * Load and call the relevant controller action.
     *
     * @param string $controller
     * @param string $action
     */
    protected function callAction($controller, $action)
    {
        $controller = "App\\Controllers\\{$controller}";
        $controller = new $controller;

        if (! method_exists($controller, $action)) {
            throw new Exception(
                "{$controller} Controller does not respond to the {$action} action."
            );
        }

        return $controller->$action();
    }
}