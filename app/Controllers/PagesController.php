<?php
namespace App\Controllers;
use App\Models\Sponsor;
use Core;
class PagesController
{
    /**
     * Show the home page.
     */
    public function home()
    {
        return view('home');
    }

    /**
     * Show the sponsors page.
     */
    public function sponsors()
    {
        $sponsors = Sponsor::getAllSponsors();
        return view('sponsors', compact('sponsors'));
    }

    /**
     * Show the speakers page.
     */
    public function speakers()
    {
        return view('speakers');
    }
    /**
    * show create sponsor form 
    */
    public function createSponsor()
    {
        return view('create_sponsor');

    }

    /**
    * To add Sponsor on database
    */
    public function addSponsor()
    {
        $fields = [
            'name' => $_POST['name'],
            'email' => $_POST['email']
        ];
        Sponsor::Create($fields);
        return redirect('sponsors');

    }
}
