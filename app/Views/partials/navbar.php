<header>
    <nav>
      <ul>
        <li><a class="active" href="/">Home</a></li>
        <li><a href="/speakers">Speakers</a></li>
        <li><a href="/sponsors">Sponsors</a></li>
      </ul>
    </nav>
</header>