<?php require('partials/header.php'); ?>
<h3>Add Sponsor</h3>
<div>
  <form action="/sponsors" method="POST">
    <label for="name">Name of the Sponsor</label>
    <input type="text" id="name" name="name" placeholder="Name">

    <label for="email">Email</label>
    <input type="email" id="email" name="email" placeholder="Email Address">
    <input type="submit" value="Submit">
  </form>
</div>

<?php require('partials/footer.php'); ?>