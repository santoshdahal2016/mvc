<?php

require 'vendor/autoload.php';
require 'core/Dispatcher.php';

use Core\{Route, Request};

Route::load('app/routes.php')->directToController(Request::uri(), Request::method());



